#ifndef GENERATORFILTERS_TauTypeFilter_H
#define GENERATORFILTERS_TauTypeFilter_H

#include "GeneratorModules/GenFilter.h"
#include "CLHEP/Vector/LorentzVector.h"

class IAtRndmGenSvc;

/// @author Simon Arnling Bååth
/// Filter based on TauTypeFilter by 
/// @author Michael Heldmann, Jan 2003
/// updated by Xin Chen, Nov. 2016
/// 
class TauTypeFilter : public GenFilter {
public:

  TauTypeFilter(const std::string& name, ISvcLocator* pSvcLocator);
  StatusCode filterInitialize();
  StatusCode filterFinalize();
  StatusCode filterEvent();

private:

  int m_Ntau;
  int m_Nleptau;
  int m_Nhadtau;
 
  bool m_ReverseFilter;
  bool m_leptau_veto; 
  bool m_hadtau_veto;
  

};

#endif
