#include "GeneratorFilters/TauTypeFilter.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "AthenaKernel/IAtRndmGenSvc.h"
#include "CLHEP/Random/RandomEngine.h"

TauTypeFilter::TauTypeFilter( const std::string& name, ISvcLocator* pSvcLocator)
  : GenFilter( name,pSvcLocator )   
{

  declareProperty( "Ntaus", m_Ntau = 1 );
  declareProperty( "Nhadtaus", m_Nhadtau = 0 );
  declareProperty( "Nleptaus", m_Nleptau = 0 );
  declareProperty( "LeptauVeto", m_leptau_veto = false );  
  declareProperty( "HadtauVeto", m_hadtau_veto = false );
  declareProperty( "ReverseFilter", m_ReverseFilter = false);
  
}


StatusCode TauTypeFilter::filterInitialize() {
   return StatusCode::SUCCESS;
}


StatusCode TauTypeFilter::filterFinalize() {
   return StatusCode::SUCCESS;
}

StatusCode TauTypeFilter::filterEvent() {
  
  
  HepMC::GenParticle *tau;
  tau = 0;
  int ntaulep = 0;
  int ntauhad = 0;


	//Go trhough all generated events
  McEventCollection::const_iterator itr;
  for (itr = events()->begin(); itr!=events()->end(); ++itr) {
       
    const HepMC::GenEvent* genEvt = (*itr);
    
    //We go through all particles of the events and look for taus

    for (HepMC::GenEvent::particle_const_iterator pitr = genEvt->particles_begin(); pitr != genEvt->particles_end(); ++pitr) {
      // Look for all taus with genstat != 3 (status 3 = a resonance)
     
      if (abs((*pitr)->pdg_id()) == 15 && (*pitr)->status() != 3) {
        tau = (*pitr);
        //std::cout << " Found tau with barcode " << tau->barcode() << " status " << (*pitr)->status() << std::endl;

        HepMC::GenVertex::particles_out_const_iterator begin = tau->end_vertex()->particles_out_const_begin();
        HepMC::GenVertex::particles_out_const_iterator end = tau->end_vertex()->particles_out_const_end();
        
              
        //All daughers of the Tau are searched. As soon as a daugher is found that uniqely identifies the tau decay type the count of that type is incremented and the loop is broken
        
        for ( ; begin != end; begin++ ) {       
					        
          if ( (*begin)->production_vertex() != tau->end_vertex() ) break;
          if ( abs( (*begin)->pdg_id() ) == 15 ) break;
          
          if ( abs( (*begin)->pdg_id() ) == 12 ) {
          	ntaulep++; 
          	break;
          }
          if ( abs( (*begin)->pdg_id() ) == 14 ) {
          	ntaulep++; 
          	break;
          }	
          
          if ( abs( (*begin)->pdg_id()) == 111 || 
          		 abs( (*begin)->pdg_id()) == 130 ||
          		 abs( (*begin)->pdg_id()) == 211 ||
          		 abs( (*begin)->pdg_id()) == 221 ||
          		 abs( (*begin)->pdg_id()) == 223 ||
          		 abs( (*begin)->pdg_id()) == 310 ||
          		 abs( (*begin)->pdg_id()) == 311 ||
          		 abs( (*begin)->pdg_id()) == 321 ||
          		 abs( (*begin)->pdg_id()) == 323 ){
          
          	ntauhad++;
          	break;
          }
          
        }
           
      }
    }
  }

	//Here we check the count of the different type of taus against the requirments. If the requirments are passed, the filter will pass.

  bool pass = (ntaulep + ntauhad >= m_Ntau && ntaulep >= m_Nleptau	&& ntauhad >= m_Nhadtau);
  
  //Here we check for if the filter is reversed or if we veto tau_had or tau_lep
  
  pass = m_ReverseFilter ? !pass : pass;
  if (m_leptau_veto && ntaulep > 0) pass = false;
	if (m_hadtau_veto && ntauhad > 0) pass = false;

	
// The filter is done!
  setFilterPassed(pass);
  return StatusCode::SUCCESS;
}
